#include <iostream>
#include <unordered_set>
#include <array>
using namespace std;

class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        if(s.empty()) {
            return 0;
        } else if(s.size() == 1) {
            return 1;
        }

        // Moving window algo
        int n = s.size();
        int maxLen = 0;
        array<int, 128> charactersVect{0};

        char currChar;
        int lowerBound = 0;
        int currLen = 0;
        for(int i = 0; i < n; ++i) {
            currChar = s[i];

            if(charactersVect[currChar] > 0) {
                if(currLen > maxLen) {
                    maxLen = currLen;
                }

                while(charactersVect[currChar] > 0) {
                    charactersVect[s[lowerBound++]] = 0;
                    --currLen;
                }
            }

            charactersVect[currChar] = 1;
            ++currLen;
        }

        // Check the last case - we've gone out of array
        if(currLen > maxLen) {
            maxLen = currLen;
        }

        return maxLen;
    }
};

int main() {
    Solution sol;

    cout << sol.lengthOfLongestSubstring("ibaz") << endl;

    return 0;
}