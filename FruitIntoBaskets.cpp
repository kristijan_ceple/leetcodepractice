#include <iostream>
#include <unordered_map>
#include <vector>
#include <queue>
using namespace std;

struct TypeData {
    int fruitType = -1;
    int lastIndexSeen = -1;
    bool set = false;
};

struct Basket {
    TypeData firstType;
    TypeData secondType;
};

class Solution {
private:

    inline void initFruitType(TypeData& fruitTypeToUpdate, int i, int currType) {
        fruitTypeToUpdate.fruitType = currType;
        fruitTypeToUpdate.lastIndexSeen = i;
        fruitTypeToUpdate.set = true;
    }

    /**
     * Returns new starting index
     * If it's equal to -1, means we move on
    */
    inline int updateFruitType(TypeData& firstType, TypeData& secondType, vector<int>& fruits, int i) {
        int currType = fruits[i];

        // Either first type or second type are set - update them respectively
        // Or we have gone out of zone with the third type
        if(currType == firstType.fruitType || currType == secondType.fruitType) {
            // One of them is applicable
            if(currType == firstType.fruitType) {
                firstType.lastIndexSeen = i;
            } else {
                secondType.lastIndexSeen = i;
            }
        } else {
            // Out of zone
            int prevType = fruits[i-1];
            
            // Find the last index of the previous type
            int lastIndex;
            if(prevType == firstType.fruitType) {
                lastIndex = secondType.lastIndexSeen + 1;
            } else {
                lastIndex = firstType.lastIndexSeen + 1;
            }

            //  That's where we'll start from next
            return lastIndex;
        }

        return -1;
    }

    int takeSubSequence(vector<int>& fruits, int startingIndex, int& newStartingIndex) {
        TypeData firstType, secondType;
        newStartingIndex = -1;

        int currType, lastType = -1, length = 0;
        for(int i = startingIndex; i < fruits.size(); ++i) {
            currType = fruits[i];

            // Check if the first type is assigned
            if(!firstType.set) {
                this->initFruitType(firstType, i, currType);
            } else if(currType != firstType.fruitType && !secondType.set) {
                this->initFruitType(secondType, i, currType);
            } else {
                int ret = this->updateFruitType(firstType, secondType, fruits, i);
                if(ret != -1) {
                    // End of line, we're done
                    length = i - startingIndex;
                    newStartingIndex = ret;
                    return length;
                }
            }

            lastType = currType;
        }

        // Handle end-of-array (edge) cases - if we made it until the end
        // One type only. vs. combined types intervals
        if(!secondType.set) {
            newStartingIndex = fruits.size();
            length = fruits.size() - startingIndex;
        } else if(newStartingIndex == -1) {
            newStartingIndex = fruits.size();
            length = fruits.size() - startingIndex;
        }

        return length;
    }
public:
    int totalFruit(vector<int>& fruits) {
        if(fruits.size() == 0) {
            return 0;
        }

        // Calculate first
        int i = 0;
        int maxSubSeq = 0;
        int newStartingIndex;
        while(i < fruits.size()) {
            int len = this->takeSubSequence(fruits, i, newStartingIndex);
            i = newStartingIndex;

            if(len > maxSubSeq) {
                maxSubSeq = len;
            }
        }

        return maxSubSeq;
    }
};

int main(void) {
    Solution sol;

    vector<int> inputData{0, 0, 1, 1};
    cout << sol.totalFruit(inputData) << endl;

    return 0;
}