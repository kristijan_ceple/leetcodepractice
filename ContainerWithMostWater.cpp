#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    int maxArea(vector<int>& height) {
        int n = height.size();
        if(n < 2) {
            return 0;
        }

        int maxArea = 0;

        int tmpArea;
        int leftBound = 0, rightBound = n-1;
        int leftHeight, rightHeight;
        int xLen = n-1;
        while(rightBound > leftBound) {
            leftHeight = height[leftBound];
            rightHeight = height[rightBound];

            tmpArea = min(leftHeight, rightHeight) * xLen;
            if(tmpArea > maxArea) {
                maxArea = tmpArea;
            }

            if(leftHeight <= rightHeight) {
                ++leftBound;
            } else {
                --rightBound;
            }

            --xLen;
        }

        return maxArea;
    }
};

int main() {
    Solution sol;

    vector<int> heights{1,8,6,2,5,4,8,3,7};
    cout << sol.maxArea(heights) << endl;

    return 0;
}