#include <iostream>
using namespace std;

void ispis(int a, int b) {
    cout << "Recursive call for: (a=" << a << ", b=" << b << ") => ENTRY" << endl;

    if(a <= b) {
        cout << "Recursive call for: (a=" << a << ", b=" << b << ") => GOING DEEPER" << endl;
        ispis(a, b-1);
        cout << "Recursive call for: (a=" << a << ", b=" << b << ") => RETURNING UP AND PRINTING CURRENT STATE:";
        cout << b << endl;
    } else {
        cout << "Recursive call for: (a=" << a << ", b=" << b << ") => NOT GOING DEEPER - CONDITION NOT SATISFIED, GO BACK UP" << endl;
    }
}

int main(void) {
    cout << "ROOT CALLING RECURSION" << endl;
    ispis(-1, 5);
    cout << "RECURSION BACK UP TO ROOT" << endl;

    return 0;
}