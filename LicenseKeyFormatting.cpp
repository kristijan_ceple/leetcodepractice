#include <iostream>
#include <sstream>
using namespace std;

class Solution {
public:
    string licenseKeyFormatting(string s, int k) {
        if(s.size() == 0) {
            return "";
        }

        // Let's get the non-dashed string first - with lowercase letters
        stringstream ss;      
        for(char currChar : s) {
            if(currChar != '-') {
                if(isdigit(currChar)) {
                    ss << currChar;
                } else {
                    char upperChar = toupper(currChar);
                    ss << upperChar;
                }
            }
        }

        string pureLicence = ss.str();

        // Let's first calculate group size
        int firstGroupSize = pureLicence.size() % k;
        if(firstGroupSize == 0) {
            firstGroupSize = k;
        }

        int n = pureLicence.size();
        stringstream finalLicence;

        // Let's do the first group
        for(int i = 0; i < firstGroupSize && i < n; ++i) {
            finalLicence << pureLicence[i];
        }

        bool insertDash = true;
        int groupCounter = 0;
        for(int i = firstGroupSize; i < n; ++i) {
            if(groupCounter == 0) {
                finalLicence << '-';
            }

            finalLicence << pureLicence[i];
            
            ++groupCounter;
            groupCounter %= k;
        }

        return finalLicence.str();
    }
};

int main(void) {
    Solution sol;

    cout << sol.licenseKeyFormatting("2-5g-3-J", 2) << endl;
}