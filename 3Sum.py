from collections import defaultdict

class Solution:
    def threeSum(self, nums):
        n = len(nums)
        if n < 3:
            return []

        results = set()
        num2Indices = defaultdict(list)

        for i, num in enumerate(nums):
            num2Indices[num].append(i)

        for i in range(n):
            currIndices = {i}
            self.twoSum(nums, i, nums[i], results, num2Indices, currIndices)

        return [list(result) for result in results]

    def oneSum(self, nums, targetSum, results, num2Indices, currIndices):
        if targetSum in num2Indices:
            for numIndex in num2Indices[targetSum]:
                if numIndex not in currIndices:
                    currIndices.add(numIndex)
                    newRes = [nums[currIndex] for currIndex in currIndices]
                    results.add(tuple(sorted(newRes)))
                    currIndices.remove(numIndex)

    def twoSum(self, nums, currIndex, sumSoFar, results, num2Indices, currIndices):
        processedTwoSums = set()
        for i in range(currIndex + 1, len(nums)):
            currTuple = (currIndex, i)
            if currTuple in processedTwoSums:
                continue

            newSum = sumSoFar + nums[i]
            newTargetSum = -newSum

            currIndices.add(i)
            self.oneSum(nums, newTargetSum, results, num2Indices, currIndices)
            currIndices.remove(i)
            processedTwoSums.add(currTuple)

# Test the code
sol = Solution()
inputData = [-15, 6, 7, 0, -14, -5, -3, -10, -14, 1, -14, -1, -11, -11, -15, -1, 3, -12, 7, 14, 1, 6, -6, 7, 1, 1, 0, -4, 8, 7, 2, 1, -2, -6, -14, -9, -3, -1, -12, -2, 7, 11, 4, 12, -14, -4, -4, 4, -1, 10, 3, -14, 1, 12, 0, 10, -9, 8, -9, 14, -8, 8, 0, -3, 10, -6, 4, -8, 0, -1, -3, -8, -4, 8, 11, -3, -11, -8, 8, 3, 10, -3, -4, -4, -14, 12, 13, -8, -3, 12, -8, 4, 5, -1, -14, -8, 8, -3, -9, -15, 12, -5, -7, -15, -12, 11, -11, 14, 11, 12, 3, 6, -6]
res = sol.threeSum(inputData)

for currResVect in res:
    print(currResVect)

print("DONE!")
