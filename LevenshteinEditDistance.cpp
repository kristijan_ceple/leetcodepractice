#include <iostream>
#include <string>
using namespace std;

class Solution {
private:
    int _solveRec(const string& first, const string& second, int firstCheckedChars, int secondCheckedChars) {
        if(firstCheckedChars == first.size()) {
            return (second.size() - secondCheckedChars);
        } else if(secondCheckedChars == second.size()) {
            return (first.size() - firstCheckedChars);
        }

        // Check if they are equal
        if(first[firstCheckedChars] == second[secondCheckedChars]) {
            return this->_solveRec(first, second, firstCheckedChars+1, secondCheckedChars+1);
        }

        // They are not equal, so let's try calculating for 3 possible paths
        return 1 +
            std::min({
                this->_solveRec(first, second, firstCheckedChars, secondCheckedChars+1),      // Insertion (into first string)
                this->_solveRec(first, second, firstCheckedChars+1, secondCheckedChars),      // Deletion (of first string character)
                this->_solveRec(first, second, firstCheckedChars+1, secondCheckedChars+1)       // Substitution
            });

    }

public:

    int solveRec(const string& first, const string& second) {
        int n = first.size(), m = second.size();
        return this->_solveRec(first, second, 0, 0);
    }
};

int main() {
    string first = "kitten";
    string second = "sitting";

    Solution sol;

    int res = sol.solveRec(first, second);
    cout << res << endl;

    return 0;
}