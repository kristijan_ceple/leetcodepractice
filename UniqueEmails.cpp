#include <iostream>
#include <vector>
#include <unordered_set>
#include <sstream>
using namespace std;

class Solution {
private:
    inline string formEffectiveEmail(int monkeyPosition, stringstream& effectiveEmailStream, string& inputEmail) {
        effectiveEmailStream << '@';
        effectiveEmailStream << inputEmail.substr(monkeyPosition+1);
        return effectiveEmailStream.str();
    }

    string getEffectiveEmail(string inputEmail) {
        //int monkeyPosition = inputEmail.find_last_of('@');
        stringstream effectiveEmailStream{};

        int i = 0;
        while(true) {
            char currChar = inputEmail[i];
            if(currChar == '@') {
                // Form the effective email
                return this->formEffectiveEmail(i, effectiveEmailStream, inputEmail);
            } else if(currChar == '+') {
                // Okay, now we have to reach the '@'
                int j = i + 1;
                while(true) {
                    currChar = inputEmail[j];
                    
                    if(currChar == '@') {
                        // Form the effective email
                        return this->formEffectiveEmail(j, effectiveEmailStream, inputEmail);
                    }

                    ++j;
                }
            } else if(currChar != '.') {
                effectiveEmailStream << currChar;
            }

            ++i;
        }

        return "!!!ERROR!!!";
    }

public:
    int numUniqueEmails(vector<string>& emails) {
        unordered_set<string> uniqueEmails;

        
        for(auto& currEmail : emails) {
            string res = this->getEffectiveEmail(currEmail);
            uniqueEmails.emplace(res);
        }

        return uniqueEmails.size();
    }
};

int main(void) {
    Solution sol;

    return 0;
}