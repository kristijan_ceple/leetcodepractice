#include <iostream>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>
#include <forward_list>
#include <list>
#include <concepts>
using namespace std;

template<typename T>
concept VectorOrList = std::same_as<T, std::vector<int>> || std::same_as<T, list<int>>;

template<typename T>
concept UnorderedMapOfTrackers = std::same_as<T, unordered_map<int, vector<list<int>::const_iterator>>> || std::same_as<T, unordered_map<int, vector<int>>>;

class Solution {
private:
    unordered_map<int, vector<int>> nums2TheirIndicesFactory(vector<int>& nums) {
        unordered_map<int, vector<int>> toRet;

        // Prepare the buckets first
        int i = 0;
        for(int currNum : nums) {
            const auto& bucket = toRet.find(currNum);
            if(bucket == toRet.end()) {
                toRet[currNum] = vector<int>{i};
            } else {
                bucket->second.emplace_back(i);
            }

            ++i;
        }

        return toRet;
    }

    unordered_map<int, vector<list<int>::const_iterator>> nums2TheirIndicesFactory(list<int>& nums) {
        unordered_map<int, vector<list<int>::const_iterator>> toRet;

        for(auto cIt = nums.cbegin(); cIt != nums.cend(); cIt++) {
            int currNum = *cIt;
            const auto& bucket = toRet.find(currNum);

            if(bucket == toRet.end()) {
                toRet[currNum] = vector<list<int>::const_iterator>{cIt};
            } else {
                bucket->second.emplace_back(cIt);
            }
        }

        return toRet;
    }

    template<UnorderedMapOfTrackers T>
    unordered_map<int, int> nums2BucketIndexFactory(T& mapToFormTrackers) {
        unordered_map<int, int> toRet;

        for(const auto& currBucket: mapToFormTrackers) {
            if(currBucket.second.size() > 1) {
                toRet.emplace(currBucket.first, 0);
            }
        }

        return toRet;
    }

    inline int getArrIndexForNum(int num
        , unordered_map<int, vector<int>>& num2Indices
        , unordered_map<int, int>& num2BucketIndices
        , unordered_map<int, int>& arrNumBaseDeletedOffset
        , bool increment, bool nextDuplicate
        ) 
    {
        const auto& numBucket = num2Indices[num];
        if(numBucket.size() > 1) {
            // Fetch the data, and increment the counter
            int numBucketIndex = num2BucketIndices[num];
            if(nextDuplicate) {
                ++numBucketIndex;
            }

            int deletionOffset = arrNumBaseDeletedOffset[num];
            int numIndexToRet = numBucket[numBucketIndex + deletionOffset];
            
            if(increment) {
                num2BucketIndices[num] = numBucketIndex + 1;
            }

            return numIndexToRet;
        } else {
            return numBucket[0];
        }
    }

    inline list<int>::const_iterator getSortedArrIndexForNum(int num 
        , unordered_map<int, vector<list<int>::const_iterator>>& num2Iters
        , unordered_map<int, int>& num2BucketIndices
        , bool increment, bool getNextDuplicate) {
        const auto& numBucket = num2Iters[num];
        if(numBucket.size() > 1) {
            // Fetch the data, and increment the counter
            int numBucketIndex = num2BucketIndices[num];
            if(getNextDuplicate) {
                ++numBucketIndex;
            }

            list<int>::const_iterator numIterToRet = numBucket[numBucketIndex];
            
            if(increment) {
                num2BucketIndices[num] = numBucketIndex + 1;
            }

            return numIterToRet;
        } else {
            return numBucket[0];
        }
    }

    inline void eraseNumFromSortedArrByIter(int num
        , list<int>& sortedArr
        , unordered_map<int, vector<list<int>::const_iterator>>& num2SortedArrIters
        , unordered_map<int, int>& num2BucketIndices
    ) {
        // Fetch its index/pointer to location (via iterator)
        list<int>::const_iterator numIter = this->getSortedArrIndexForNum(num, num2SortedArrIters, num2BucketIndices, true, false);
        
        // First get it out of the list
        sortedArr.erase(numIter);

        // Bucket index has been moved, and the element has been erased
        // Iterators to other numbers in the sorted array ARE NOT invalidated, because we're working with a double-linked list :D
    }

    inline void eraseNumFromSortedArr(int num, list<int>& sortedArr, unordered_map<int, int>& arrNum2BucketIndex) {
        for(auto it = sortedArr.begin(); it != sortedArr.end(); it++) {
            if(*it == num) {
                sortedArr.erase(it);
                break;
            }
        }

        const auto& foundBucket = arrNum2BucketIndex.find(num);
        if(foundBucket != arrNum2BucketIndex.cend()) {
            int currBucketIndex = foundBucket->second;
            arrNum2BucketIndex[num] = (++currBucketIndex);
        }
    }

    bool goodStartingIndices(const vector<int>& arr, list<int> sortedArr, int startingIndex, unordered_map<int, vector<int>>& num2ArrIndices
        , unordered_map<int, int>& arrNumBaseDeletedOffset 
        // , unordered_map<int, vector<list<int>::const_iterator>>& num2SortedArrIterators, unordered_map<int, int> num2BucketIndices
        ) {
        // TODO: Try to make num2ArrIndices const

        if(startingIndex == arr.size()) {
            return true;
        }

        // Prepare supporting data structures
        unordered_map<int, vector<list<int>::const_iterator>> num2SortedArrIters = this->nums2TheirIndicesFactory(sortedArr);
        unordered_map<int, int> num2SortedArrBucketIndices = this->nums2BucketIndexFactory(num2SortedArrIters);

        int lastIndexBeforeJump;
        int i = startingIndex;
        int currJumpIndex = 1;
        while(true) {
            // Check if we've arrived at the destination, or gone out of bounds
            if(i == (arr.size()-1)) {
                return true;
            } else if(sortedArr.size() <= 1) {
                return false;
            }
            
            lastIndexBeforeJump = i;

            int currNum = arr[i];
            list<int>::const_iterator currNumSortedArrIter = this->getSortedArrIndexForNum(currNum, num2SortedArrIters, num2SortedArrBucketIndices, false, false);

            bool isOdd = currJumpIndex % 2;
            int nextNum, nextNumIndex;

            if(isOdd) {
                // Odd -> jump to next smallest (greater than or euqal to the current) number, and located to the right
                ++currNumSortedArrIter;
               
                // Check if we can even go to the right
                if(currNumSortedArrIter == sortedArr.cend()) {
                    return false;
                }

                nextNum = *(currNumSortedArrIter);
            } else {
                // Even -> Jump to the next greatest (smaller than or equal to the current) number, and located to the right
                
                // Check if maybe duplicate can be found to the right!
                if(++currNumSortedArrIter != sortedArr.cend() && (*currNumSortedArrIter) == currNum ) {
                    nextNum = *currNumSortedArrIter;
                } else {
                    --currNumSortedArrIter;
                }
                
                if(currNumSortedArrIter == sortedArr.begin()) {
                    return false;
                }

                nextNum = *(--currNumSortedArrIter);
            }

            // If nextNum is the same as currNum, we are dealing with duplicates - be extra careful here
            if(nextNum != currNum) {
                nextNumIndex = this->getArrIndexForNum(nextNum, num2ArrIndices, num2SortedArrBucketIndices, arrNumBaseDeletedOffset, false, false);
            } else {
                nextNumIndex = this->getArrIndexForNum(nextNum, num2ArrIndices, num2SortedArrBucketIndices, arrNumBaseDeletedOffset, false, true);
            }

            // Okay, now we can move i to the current number
            i = nextNumIndex;
            ++currJumpIndex;
            
            // Remove all numbers from last jump to i
            for(int j = lastIndexBeforeJump; j < i; j++) {
                int numToDelete = arr[j];
                this->eraseNumFromSortedArrByIter(numToDelete, sortedArr, num2SortedArrIters, num2SortedArrBucketIndices);
            }
        }
    }

public:
    // This is the entry method for the algorithm
    int oddEvenJumps(vector<int>& arr) {
        // Temporary buffer vector for sorting
        vector<int> sortedArrVector = arr;
        sort(sortedArrVector.begin(), sortedArrVector.end());
        
        // Morph buffer vector into list
        list<int> sortedArr;
        for(int currNum : sortedArrVector) {
            sortedArr.emplace_back(currNum);
        }

        // Get rid of the buffer vector
        sortedArrVector.clear();

        // Form the supporting data structures
        unordered_map<int, vector<int>> num2ArrIndices = this->nums2TheirIndicesFactory(arr);
        unordered_map<int, int> arrNum2BaseOffset = this->nums2BucketIndexFactory(num2ArrIndices);

        // Main algorithm part
        int goodIndicesCounter = 0;
        for(int i = 0; i < arr.size(); i++) {
            if(this->goodStartingIndices(arr, sortedArr, i, num2ArrIndices, arrNum2BaseOffset)) {
                ++goodIndicesCounter;
            }

            // We can eliminate the current number from the sorted list here
            this->eraseNumFromSortedArr(arr[i], sortedArr, arrNum2BaseOffset);
        }

        return goodIndicesCounter;
    }
};