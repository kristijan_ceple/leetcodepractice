#include <string>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <cstdint>
using namespace std;

class Solution {
public:
    inline int digitChar2Int(char digit) {
        switch(digit) {
            case '0': {
                return 0;
            }
            case '1': {
                return 1;
            }
            case '2': {
                return 2;
            }
            case '3': {
                return 3;
            }
            case '4': {
                return 4;
            }
            case '5': {
                return 5;
            }
            case '6': {
                return 6;
            }
            case '7': {
                return 7;
            }
            case '8': {
                return 8;
            }
            case '9': {
                return 9;
            }
            default:
                throw invalid_argument("Unallowed digit! Char: " + std::to_string(digit));
        }
    }

    inline char digitInt2Char(int digit) {
        switch(digit) {
            case 0: {
                return '0';
            }
            case 1: {
                return '1';
            }
            case 2: {
                return '2';
            }
            case 3: {
                return '3';
            }
            case 4: {
                return '4';
            }
            case 5: {
                return '5';
            }
            case 6: {
                return '6';
            }
            case 7: {
                return '7';
            }
            case 8: {
                return '8';
            }
            case 9: {
                return '9';
            }
            default:
                throw invalid_argument("Only digits allowed! Int called: " + std::to_string(digit));
        }
    }

    int str2Num(const string& numStr) {
        int res = 0;

        for(char currChar : numStr) {
            res *= 10;
            res += digitChar2Int(currChar);

        }

        return res;
    }

    string num2Str(int num) {
        stringstream ss;

        int currDigit;
        while(num > 0) {
            currDigit = num % 10;
            ss << digitInt2Char(currDigit);
            num /= 10;
        }


        string res = ss.str();
        std::reverse(res.begin(), res.end());
        return res;
    }

    string multiplyStrWithChar(const string& numInput, char currChar) {
        stringstream ss;

        char currChar2;
        int carryOver = 0, tmpMul;
        int currDigit = digitChar2Int(currChar), currDigit2;
        for(int i = numInput.size() - 1; i >= 0; --i) {
            currChar2 = numInput[i];
            currDigit2 = digitChar2Int(currChar2);

            tmpMul = currDigit * currDigit2;
            tmpMul += carryOver;

            if(tmpMul > 9) {
                carryOver = tmpMul / 10;
                tmpMul %= 10;
            } else {
                carryOver = 0;
            }

            ss << tmpMul;
        }

        // After the loop - check if there was anything in the carryover
        if(carryOver > 0) {
            ss << digitInt2Char(carryOver);
        }

        string res = ss.str();
        std::reverse(res.begin(), res.end());
        return res;
    }

    string add2Strings(const string& str1, const string& str2) {
        stringstream ss;
        ss << '0';

        int str1Len = str1.size();
        int str2Len = str2.size();

        int str1Index = str1Len - 1;
        int str2Index = str2Len - 1;

        char currCh1, currCh2;
        int currInt1, currInt2;
        int tmpAdd, carryOver = 0;
        while(true) {
            if(str1Index < 0 && str2Index < 0) {
                    break;
            }

            tmpAdd = carryOver;
            if(str1Index >= 0) {
                currCh1 = str1[str1Index];
                currInt1 = digitChar2Int(currCh1);

                tmpAdd += currInt1;

                --str1Index;
            }

            if(str2Index >= 0) {
                currCh2 = str2[str2Index]; 
                currInt2 = digitChar2Int(currCh2);
                
                tmpAdd += currInt2;

                --str2Index;
            }

            if(tmpAdd > 9) {
                carryOver = 1;
                tmpAdd -= 10;
            } else {
                carryOver = 0;
            }

            ss << tmpAdd;
        }

        // At the end - we just have to check the carry over
        if(carryOver > 0) {
            ss << '1';
        }

        string res = ss.str();
        std::reverse(res.begin(), res.end());
        return res;
    }

    string multiply(string num1, string num2) {
        if(num1 == "0" || num2 == "0") {
            return "0";
        } else if(num1 == "1") {
            return num2;
        } else if(num2 == "1") {
            return num1;
        }
        
        char currChar;
        string accumulator = "0", tmp;
        for(int i = 0; i < num2.size(); ++i) {
            currChar = num2[i];
            tmp = std::move(multiplyStrWithChar(num1, currChar));
            accumulator = std::move(add2Strings(accumulator, tmp));
        }
        
        accumulator.pop_back();
        
        return accumulator;
    }
};

int main(void) {
    Solution sol;

    /*
     * "498828660196"
     * "840477629533"
     */

    string num1 = "498828660196";
    string num2 = "840477629533";

    // string num1 = "2";
    // string num2 = "3";
    
    string res = sol.multiply(num1, num2);
    cout << "Result = " << res << endl;

    return 0;
}