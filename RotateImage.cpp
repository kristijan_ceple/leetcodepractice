#include <vector>
#include <iostream>
using namespace std;

class Solution {
public:
    inline void rotateLayer(vector<vector<int>>& matrix, int n, int x, int y) {
        int tmp, rotatedNum;
        int xPrime, yPrime;

        // First rotation
        xPrime = y;
        yPrime = (n - 1 - x);

        rotatedNum = matrix[xPrime][yPrime];
        matrix[xPrime][yPrime] = matrix[x][y];

        // Second rot
        xPrime = (n - 1 - x);
        yPrime = (n - 1 - y);

        tmp = matrix[xPrime][yPrime];
        matrix[xPrime][yPrime] = rotatedNum;
        rotatedNum = tmp;

        // Third rot
        xPrime = n-1-y;
        yPrime = x;

        tmp = matrix[xPrime][yPrime];
        matrix[xPrime][yPrime] = rotatedNum;
        rotatedNum = tmp;

        // Last Rot
        matrix[x][y] = rotatedNum;
    }

    void rotate(vector<vector<int>>& matrix) {
        int n = matrix.size();

        for(int i = 0; i < n/2; ++i) {
            for(int j = i; j < (n-i-1); ++j) {
                rotateLayer(matrix, n, i, j);
            }
        }
    }
};

int main(void) {
    Solution sol;

    vector<vector<int>> inputData {
        {5,1,9,11},
        {2,4,8,10},
        {13,3,6,7},
        {15,14,12,16}
    };

    sol.rotate(inputData);

    int n = inputData.size();
    for(int i = 0; i < n; ++i) {
        for(int j = 0; j < n; ++j) {
            cout << inputData[i][j] << " ";
        }

        cout << endl;
    }

    return 0;
}