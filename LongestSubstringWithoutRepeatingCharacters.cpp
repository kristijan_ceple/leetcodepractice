#include <iostream>
#include <unordered_set>
using namespace std;

class Solution {
public:
    int lengthOfLongestSubstring(string s) {
        if(s.empty()) {
            return 0;
        } else if(s.size() == 1) {
            return 1;
        }

        // Moving window algo
        int n = s.size();
        int maxLen = 0;
        unordered_set<char> charsSet;

        char currChar;
        int lowerBound = 0;
        for(int i = 0; i < n; ++i) {
            currChar = s[i];

            if(charsSet.contains(currChar)) {
                if(charsSet.size() > maxLen) {
                    maxLen = charsSet.size();
                }

                while(charsSet.contains(currChar)) {
                    charsSet.erase(s[lowerBound++]);                    
                }
            }

            charsSet.emplace(currChar);
        }

        // Check the last case - we've gone out of array
        if(charsSet.size() > maxLen) {
            maxLen = charsSet.size();
        }

        return maxLen;
    }
};

int main() {
    Solution sol;

    cout << sol.lengthOfLongestSubstring("pwwkew") << endl;

    return 0;
}