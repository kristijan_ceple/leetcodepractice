#include <iostream>
#include <unordered_set>
using namespace std;

class Solution {
private:
    // unordered_set<pair<int, int>> palindromes;
    int maxPalindromeLen = 0, maxPalindromeIndex = -1;        

    /**
     * [beginIndex, endIndex]
     * 
     * Check those 2 and expand outward
     */
    inline void checkIfPalindrome(string& s, int n, int beginIndex, int endIndex) {
        char leftChar, rightChar;
        int currLen;

        while(beginIndex >= 0 && endIndex < n) {
            leftChar = s[beginIndex], rightChar = s[endIndex];
            currLen = endIndex - beginIndex + 1;

            if(leftChar == rightChar) {
                // Note it down and extend
                if(currLen > maxPalindromeLen) {
                    maxPalindromeLen = currLen; maxPalindromeIndex = beginIndex;
                }

                --beginIndex; ++endIndex;
            } else {
                return;
            }
        }
    }
public:
    string longestPalindrome(string s) {
        /*
            We assume all strings of length 0 and 1 are palindromes
        */
        int n = s.size();

        if(n == 0 || n == 1) {
            return std::move(s);
        }

        char currChar, nextChar;
        for(int i = 0; i < n-1; ++i) {
            currChar = s[i];
            nextChar = s[i+1];

            if(currChar == nextChar) {
                // Palindrome of length 2 - try to expand further to the left and right
                // Possible even-len palindrome

                // Note it down
                if(2 > maxPalindromeLen) {
                    maxPalindromeLen = 2; maxPalindromeIndex = i;
                }

                // Check if a wider palindrome is possible
                if(i > 0) {
                    checkIfPalindrome(s, n, i-1, i+2);
                }
            }

            // Palindrome of length 1 - try to expand further to the left and right
            // Possible odd-len palindrome (with a centre char)
            
            // Check if a wider palindrome is possible
            if(i > 0) {
                checkIfPalindrome(s, n, i-1, i+1);
            }
        }

        if(maxPalindromeIndex != -1) {
            return s.substr(maxPalindromeIndex, maxPalindromeLen);
        } else {
            return string(1, s[0]);
        }
    }
};

int main(void) {
    Solution sol;

    string inputData = "asdasdasdasdasuiadauidbaiudbajkwbduidiunduwiqdnuwbuiefbasbjaksdjokasdjasojkfnaijfjaoisdjasokdasijdhasoidjasodnaijdasjoiasndaoindasoidnasoidjnmasoidasjnoujfansoasjndoasindaoisndasoidnasoiasdasdasdasdasuiadauidbaiudbajkwbduidiunduwiqdnuwbuiefbasbjaksdjokasdjasojkfnaijfjaoisdjasokdasijdhasoidjasodnaijdasjoiasndaoindasoidnasoidjnmasoidasjnoujfansoasjndoasindaoisndasoidnasoiasdasdasdasdasuiadauidbaiudbajkwbduidiunduwiqdnuwbuiefbasbjaksdjokasdjasojkfnaijfjaoisdjasokdasijdhasoidjasodnaijdasjoiasndaoindasoidnasoidjnmasoidasjnoujfansoasjndoasindaoisndasoidnasoiasdasdasdasdasuiadauidbaiudbajkwbduidiunduwiqdnuwbuiefbasbjaksdjokasdjasojkfnaijfjaoisdjasokdasijdhasoidjasodnaijdasjoiasndaoindasoidnasoidjnmasoidasjnoujfansoasjndoasindaoisndasoidnasoiasdasdasdasdasuiadauidbaiudbajkwbduidiunduwiqdnuwbuiefbasbjaksdjokasdjasojkfnaijfjaoisdjasokdasijdhasoidjasodnaijdasjoiasndaoindasoidnasoidjnmasoidasjnoujfansoasjndoasindaoisndasoidnasoi";
    string res = sol.longestPalindrome(inputData);
    
    cout << "Input string: " << inputData << endl;
    cout << "Res: " << res << endl;

    return 0;
}