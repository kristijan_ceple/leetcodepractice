#include <iostream>
#include <vector>
using namespace std;

class Solution {
private:
    // vector<int> canJumpToTable;

    // inline bool canJumpToIndexBackward(vector<int>& nums, int currIndex) {
    //     if(currIndex == 0) {
    //         return true;
    //     }

    //     // Find all indices that can jump to curr index
    //     for(int i = 0; i < currIndex; ++i) {
    //         if(canJumpToTable[i] == -1) {
    //             continue;
    //         }
            
    //         int currNum = nums[i];
            
    //         if(currNum + i >= currIndex) {
    //             if(canJumpToIndexBackward(nums, i)) {
    //                 return true;
    //             }
    //         }
    //     }

    //     canJumpToTable[currIndex] = -1;
    //     return false;
    // }

    // inline bool canJumpToIndexForward(vector<int>& nums, int currIndex) {
    //     if(currIndex == n-1) {
    //         return true;        // Base case
    //     }

    //     int maxJumpIndex = currIndex + nums[currIndex];
    //     if(maxJumpIndex >= n) {
    //         maxJumpIndex = n-1;     // Clamping
    //     }

    //     for(int i = currIndex+1; i <= maxJumpIndex; ++i) {
    //         if(canJumpToTable[i] == -1) {
    //             continue;
    //         }

    //         if(canJumpToIndexForward(nums, i)) {
    //             return true;
    //         } else {
    //             canJumpToTable[i] = -1;
    //         }
    //     }

    //     canJumpToTable[currIndex] = -1;
    //     return false;
    // }

public:
    bool canJump(vector<int>& nums) {
        int n = nums.size();

        // Preliminary checks
        if(n == 0) {
            return false;
        } else if(n == 1) {
            return true;
        } else if(nums[0] >= n-1) {
            return true;
        } else if(nums[0] == 0) {
            return false;
        }

        // Init the table
        // for(int i = 0; i < n; ++i) {
        //     canJumpToTable.emplace_back(0);
        // }

        // Algo and res
        int lineOfSight = nums[0];

        int tmp;
        for(int i = 1; i < n; ++i) {
            tmp = i + nums[i];
            if(tmp > lineOfSight) {
                lineOfSight = tmp;

                if(lineOfSight >= n-1) {
                    return true;
                }
            }

            if(lineOfSight <= i) {
                return false;
            }
        }

        return false;
    }
};

int main(void) {
    Solution sol;

    vector<int> inputData{2,0,6,9,8,4,5,0,8,9,1,2,9,6,8,8,0,6,3,1,2,2,1,2,6,5,3,1,2,2,6,4,2,4,3,0,0,0,3,8,2,4,0,1,2,0,1,4,6,5,8,0,7,9,3,4,6,6,5,8,9,3,4,3,7,0,4,9,0,9,8,4,3,0,7,7,1,9,1,9,4,9,0,1,9,5,7,7,1,5,8,2,8,2,6,8,2,2,7,5,1,7,9,6};
    // vector<int> inputData{2,3,1,1,4};
    bool res = sol.canJump(inputData);

    if(res) {
        cout << "True!" << endl;
    } else {
        cout << "False!" << endl;
    }

    return 0;
}