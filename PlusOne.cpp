#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    vector<int> plusOne(vector<int>& digits) {
        vector<int> results;
        
        int n = digits.size();
        if(n == 0) {
            digits = {1};
        }
        
        int carryOver = 1;
        int i;
        for(i = n-1; i >= 0 && carryOver == 1; --i) {
            ++digits[i];

            if(digits[i] >= 10) {
                digits[i] -= 10;
                carryOver = 1;
            } else {
                carryOver = 0;
            }

            results.emplace_back(digits[i]);
        }

        if(carryOver == 1) {
            // Check if carryOver is still 1 - at the end of the array
            results.emplace_back(1);
        } else {
            while(i >= 0) {
                results.emplace_back(digits[i]);
                --i;
            }
        }

        std::reverse(results.begin(), results.end());

        return results;
    }
};

int main(void) {
    Solution sol;

    vector<int> inputData{9};
    vector<int> res = sol.plusOne(inputData);

    cout << "Results: " << endl;
    for(int currNum : res) {
        cout << currNum;
    }
    cout << endl;

    return 0;
}