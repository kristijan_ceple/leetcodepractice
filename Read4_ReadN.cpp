#include <iostream>
#include <vector>
#include <deque>
#include <queue>
#include <array>
using namespace std;

/**
 * The read4 API is defined in the parent class Reader4.
 *     int read4(char *buf4);
 */

class Solution {
private:
    // deque<char> file;

    // int read4(char* buf4) {
    //     int n = 4;
    //     if(file.size() < 4) {
    //         n = file.size();
    //     }
        
    //     char tmp;
    //     for(int i = 0; i < n; ++i) {
    //         tmp = file.front();
    //         file.pop_front();
    //         buf4[i] = tmp;
    //     }

    //     return n;
    // }

    queue<char> leftOverCharsBuffer;

public:
    // Solution(string inputData) {
    //     for(char c : inputData) {
    //         file.emplace_back(c);
    //     }
    // }

    /**
     * @param buf Destination buffer
     * @param n   Number of characters to read
     * @return    The number of actual characters read
     */
    int read(char *buf, int n) {
        // Have to use read4
        if(n == 0) {
            return 0;
        }

        int currIndex = 0;
        char tmpChar;

        // First check if there were any remaining left-overs in the buffer from the previous reads
        int storedCharsInLOBuf = leftOverCharsBuffer.size();
        if(storedCharsInLOBuf > 0) {
            char tmpChar;
            for(; currIndex < n && currIndex < storedCharsInLOBuf; ++currIndex) {
                tmpChar = leftOverCharsBuffer.front();
                leftOverCharsBuffer.pop();
                buf[currIndex] = tmpChar;
            }
        }

        int remainingReads = n - currIndex;
        if(remainingReads <= 0) {
            return currIndex;
        }

        // Now we go on to reading from the file
        int fullReads = remainingReads / 4;
        int moduloReads = remainingReads % 4;

        // Full reads first
        int tmpCnt;
        for(int i = 0; i < fullReads; ++i) {
            tmpCnt = read4(buf+currIndex);

            currIndex += tmpCnt;

            if(tmpCnt < 4) {
                return currIndex;
            }
        }


        // Last read processing
        if(moduloReads > 0) {
            // This method will read 4 chars or less. The remainder/module num may be less than, equal to, or greater than the read num of chars from the file
            // Read what you can - the unused remainder has to be saved into internal queue buffer
            array<char, 4> tmpBuf;
            tmpCnt = read4(tmpBuf.begin());

            for(int i = 0; i < tmpCnt && i < moduloReads; ++i) {
                buf[currIndex++] = tmpBuf[i];
            }

            // Excess has to be saved into the internal queue buffer
            for(int i = moduloReads, j = 0; i < tmpCnt; ++i, ++j) {
                leftOverCharsBuffer.emplace(tmpBuf[i]);
            }

            storedCharsInLOBuf = tmpCnt - moduloReads;
        }

        return currIndex;
    }
};

void testRead(Solution& sol, char* buf, int quant) {
    int n = sol.read(buf, quant);
    cout << "Read: " << n << endl;

    cout << "Buf: ";
    for(int i = 0; i < n; ++i) {
        cout << buf[i];
    }
    cout << endl;
}

int main() {
    Solution sol("abc");
    char buf[1000];

    testRead(sol, buf, 1);
    testRead(sol, buf, 1);
    testRead(sol, buf, 1);
    testRead(sol, buf, 1);

    return 0;
}