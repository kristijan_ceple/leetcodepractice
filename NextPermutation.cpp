#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    int n;

    void nextPermutation(vector<int>& nums) {
        /*
         * Go from the back - and stop immediately when the ascending order (right-to-left) is broken
         * Find the minimum number from the rupture point (min num greater than rapture num)
         * Swap the rupture and the minimum num
         * Sort the container from the rupture point until the end
         */
        n = nums.size();

        if(n < 1) {
            return;
        }

        int lastPos = n - 1;
        int currNum;
        int lastNum;

        int minNum = nums[lastPos], minNumIndex = lastPos;
        for(int i = lastPos - 1; i >= 0; --i) {
            currNum = nums[i];
            lastNum = nums[i+1];
            
            if(currNum < lastNum) {
                // Algo
                // Find minimum num
                minNum = INT32_MAX;
                minNumIndex = -1;
                for(int j = lastPos; j > i; --j) {
                    if(nums[j] < minNum && nums[j] > currNum) {
                        minNum = nums[j];
                        minNumIndex = j;
                    }
                }

                // Swap rapture and minimum num
                std::swap(nums[i], nums[minNumIndex]);
                
                // Sort from rapture point to end of array
                std::sort(nums.begin() + i+1, nums.end());

                return;
            }
        }

        // If we have come here - that means that a cycle has been reached (algo has never triggered)
        std::reverse(nums.begin(), nums.end());
    }
};

ostream& operator<<(ostream& out, const vector<int>& vect) {
    int lastPos = vect.size() - 1;
    int i  = 0;

    out << '[';
    for(int currNum : vect) {
        out << currNum;

        if(i != lastPos) {
            out << ',';
        }

        ++i;
    }
    out << ']';

    return out;
}

int main() {
    Solution sol;
    vector<int> inputData{1,2,3};

    cout << inputData << endl;

    for(int i = 0; i < 6; ++i) {
        sol.nextPermutation(inputData);
    
        // Display
        cout << inputData << endl;
    }

    // sol.nextPermutation(inputData);

    // Display
    cout << inputData << endl;
    
    return 0;
}